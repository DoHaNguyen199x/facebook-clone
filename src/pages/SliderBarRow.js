import '../style/SliderBarRow.scss'
import {Avatar} from '@material-ui/core'
import { SvgIcon } from '@mui/material';
const SliderBarRow = (props) =>{
return(
    <div className="sliderBarRow">
        {props.src && <Avatar src={props.src} style ={{transform :"scale(0.8)",fontWeight:"600"}} />}
        {props.Icon && <SvgIcon component = {props.Icon} style ={{color:"blue"}} />}
        <h4>{props.title}</h4>
    </div>
)
}
export default SliderBarRow;