import '../style/App.scss';
import Header from './Header';
import SliderBar from './Sliderbar';
import Feed from './Feed';
import Widgets from './Widgets';
import Modals from './Modal';
// const handleOpen = {};
const App = () => {
  return (
    <div className='app'>
      <Modals />
      <Header />
      <div className='app_body'>
        <SliderBar />
        <Feed />
        <Widgets />
      </div>
    </div>
  );
}
// export { handleOpen };
export default App;
