import "../style/SliderBar.scss"
import SliderBarRow from "./SliderBarRow";
import doha from '../components/doha.jpg'
import PeopleIcon from '@mui/icons-material/People';
import RestoreIcon from '@mui/icons-material/Restore';
import BookmarkIcon from '@mui/icons-material/Bookmark';
import GroupsIcon from '@mui/icons-material/Groups';
import StorefrontIcon from '@mui/icons-material/Storefront';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
const SliderBar = (props) => {
    return (
    <div className="sliderbar">
        <SliderBarRow title ="Đỗ Hà" src ={doha} />
        <SliderBarRow title = "Friends" Icon ={PeopleIcon} />
        <SliderBarRow title = "Memories" Icon ={RestoreIcon} />
        <SliderBarRow title = "Saved" Icon ={BookmarkIcon} />
        <SliderBarRow title = "Groups" Icon = {GroupsIcon} />
        <SliderBarRow title = "Marketplace" Icon = {StorefrontIcon} />
        <SliderBarRow title = "See More" Icon = {ExpandMoreIcon} />
    </div>
)
}
export default SliderBar;