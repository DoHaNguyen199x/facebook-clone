import '../style/Widgets.scss';
import { Avatar } from '@material-ui/core'
import doha from '../components/doha.jpg';
import NotificationsNoneOutlinedIcon from '@mui/icons-material/NotificationsNoneOutlined';
import CampaignOutlinedIcon from '@mui/icons-material/CampaignOutlined';
import CakeIcon from '@mui/icons-material/Cake';
import VideoCallIcon from '@mui/icons-material/VideoCall';
import SearchIcon from '@mui/icons-material/Search';
import Friends from './Friends';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
const Widgets = () => {
    return (
        <div className='widgets'>
            <div className='group_info'>
                <div className='pages'>
                    <h3>Your page and profile</h3>
                    <MoreHorizIcon style={{color:'gray'}} className='icon' />
                </div>
                <div className='group'>
                    <Avatar className='image_custom' src={doha} />
                    <b>Đỗ Hà Pages</b>
                </div>
                <div className='group_icon'>
                    <NotificationsNoneOutlinedIcon style={{color:'gray'}} />
                    <p>Notification</p>
                </div>
                <div className='group_icon'>
                    <CampaignOutlinedIcon style={{color:'gray'}} />
                    <p>Advertisement</p>
                </div>
            </div>
            <div className='Birthday'>
                <h3>Birthday</h3>
                <div className='birthdays'>
                    <CakeIcon style={{ color: '#ff82da' }} />
                    <p>Today is <strong>Do Ha</strong> birthday.</p>
                </div>
            </div>
            <div className='friends_group'>
                <div className='friendIcon'>
                    <h3 style={{ color: "gray", paddingTop: "5px" }}>Friends</h3>
                    <div className='Icons_friendsss'>
                        <VideoCallIcon style={{color:'gray'}} className='icon_friend' />
                        <SearchIcon style={{color:'gray'}} className='icon_friend' />
                        <MoreHorizIcon style={{color:'gray'}} className='icon_friend' />
                    </div>
                </div>
                <div className='friend_u'>
                    <Friends image={doha} name="Nguyen Van A" />
                    <Friends image={doha} name="Nguyen Van B" />
                    <Friends image={doha} name="Nguyen Van C" />
                    <Friends image={doha} name="Nguyen Van D" />
                    <Friends image={doha} name="Nguyen Van E" />
                </div>

            </div>
        </div>
    )
}
export default Widgets;