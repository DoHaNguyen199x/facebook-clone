import '../style/Modal.scss';
import Modal from '@mui/material/Modal';
import { useState } from 'react';
import { Avatar } from '@material-ui/core'
import doha from '../components/doha.jpg'
import PublicIcon from '@mui/icons-material/Public';
import CancelOutlinedIcon from '@mui/icons-material/CancelOutlined';
import FontDownloadOutlinedIcon from '@mui/icons-material/FontDownloadOutlined';
import SentimentSatisfiedOutlinedIcon from '@mui/icons-material/SentimentSatisfiedOutlined';
import CollectionsIcon from '@mui/icons-material/Collections';
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import EmojiEmotionsOutlinedIcon from '@mui/icons-material/EmojiEmotionsOutlined';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import FlagIcon from '@mui/icons-material/Flag';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
const handleOpen = {};
const Modals = () => {
    const [open, setOpen] = useState(false);
    handleOpen.open = () => setOpen(true);
    handleOpen.close = () => setOpen(false);
    return (
        <>
            <Modal
                open={open}
                onClose={handleOpen.close}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <div className='modal'>
                    <div className='header_custom'>
                        <h4>New posts</h4>
                        <CancelOutlinedIcon className='cancel' onClick={handleOpen.close} />
                    </div>
                    <div className='body'>
                        <div className='user_info'>
                            <Avatar src={doha} />
                            <div className='info'>
                                <h4>Đỗ Hà</h4>
                                <div className='public'>
                                    <PublicIcon />
                                    <span>Public</span>
                                </div>
                            </div>
                        </div>
                        <div className='input_custom'>
                            <input className='input' placeholder="What you're thinking ???" />
                        </div>
                        <div className='text_custom'>
                            <FontDownloadOutlinedIcon className='FontIcon' />
                            <SentimentSatisfiedOutlinedIcon className='SentIcon' />
                        </div>
                        <div className='custom_posts'>
                            <h5>Add to your post</h5>
                            <div className='icon_posts'>
                                <CollectionsIcon className='a' />
                                <GroupAddIcon className='b' />
                                <EmojiEmotionsOutlinedIcon className='c' />
                                <LocationOnIcon className='d' />
                                <FlagIcon className='e' />
                                <MoreHorizIcon className='f' />
                            </div>
                        </div>
                        <div className='Post'>
                            <p className='post_button'>Post</p>
                        </div>
                    </div>

                </div>
            </Modal>
        </>
    )
}
export { handleOpen };
export default Modals;