import '../style/Friends.scss';
import { Avatar } from '@material-ui/core';
const Friends = (props) => {
    return (
        <div className="Friends">
            <Avatar className='image' src={props.image} />
            <h4>{props.name}</h4>
        </div>
    )
}
export default Friends;