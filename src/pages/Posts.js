import "../style/Posts.scss";
import {Avatar} from '@material-ui/core'
import ThumbUpOutlinedIcon from '@mui/icons-material/ThumbUpOutlined';
import ChatBubbleOutlineOutlinedIcon from '@mui/icons-material/ChatBubbleOutlineOutlined';
import ReplyOutlinedIcon from '@mui/icons-material/ReplyOutlined';
import PublicIcon from '@mui/icons-material/Public';
const Posts = (props) => {
    return (
        <div className="posts">
            <div className="posts_top">
                <Avatar style={{cursor: 'pointer'}} src={props.userImage} />
                <div className="posts_topInfo">
                    <h3 style={{cursor: 'pointer'}}>{props.userName}</h3>
                    <div  className="date">
                    <p style={{cursor: 'pointer'}}>5 hours.</p>
                    <PublicIcon className="publicIcon" />
                    </div>
                </div>
            </div>
            <div className="posts_bottom">
                <p>{props.message}</p>
            </div>
            <div className="posts_image">
               <img style={{cursor: 'pointer'}} src={props.image} alt = "" />
            </div>
            <div className="posts_options">
                <div className="posts_option">
                    <ThumbUpOutlinedIcon />
                    <p>Like</p>
                </div>
                <div className="posts_option">
                    <ChatBubbleOutlineOutlinedIcon />
                    <p>Comments</p>
                </div>
                <div className="posts_option">
                    <ReplyOutlinedIcon />
                    <p>Share</p>
                </div>
            </div>
        </div>
    )

}
export default Posts;