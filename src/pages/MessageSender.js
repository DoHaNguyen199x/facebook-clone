import "../style/MessageSender.scss";
import { Avatar } from '@material-ui/core';
import doha from "../components/doha.jpg";
import VideocamIcon from '@mui/icons-material/Videocam';
import PhotoLibraryIcon from '@mui/icons-material/PhotoLibrary';
import MoodIcon from '@mui/icons-material/Mood';
// import { handleOpen } from "./App";
import { handleOpen } from "./Modal";
const MessageSender = () => {
    return (
    
        <div className="messageSender">
            <div className="messageSender_top">
                <Avatar style={{ cursor: 'pointer' }} src={doha} />
                <form>
                    <input
                        onClick={handleOpen.open}
                        className="messageSender_input" placeholder="What's on your mind?" />

                    <button type="submit" hidden />
                </form>
            </div>
            <div className="messageSender_bottom">
                <div className="messageSender_option">
                    <VideocamIcon style={{ color: 'red' }} />
                    <h5>Live Video</h5>
                </div>
                <div className="messageSender_option">
                    <PhotoLibraryIcon style={{ color: 'green' }} />
                    <h5>Photo/Video</h5>
                </div>
                <div className="messageSender_option">
                    <MoodIcon style={{ color: 'orange' }} />
                    <h5>Feeling/Activity</h5>
                </div>
            </div>
        </div>
    )
}
export default MessageSender;