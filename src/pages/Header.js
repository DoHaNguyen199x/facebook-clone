import '../style/Header.scss';
import SearchIcon from '@mui/icons-material/Search';
import HomeIcon from '@mui/icons-material/Home';
import OndemandVideoIcon from '@mui/icons-material/OndemandVideo';
import StorefrontIcon from '@mui/icons-material/Storefront';
import GroupsIcon from '@mui/icons-material/Groups';
import SportsEsportsIcon from '@mui/icons-material/SportsEsports';
import AppsIcon from '@mui/icons-material/Apps';
import MessageIcon from '@mui/icons-material/Message';
import doha from '../components/doha.jpg'
import NotificationsIcon from '@mui/icons-material/Notifications';
const Header = () => {
    return (
        <div className="header">
            <div className='header_left'>
                <img style={{cursor: 'pointer'}} src='https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Facebook_Logo_%282019%29.png/800px-Facebook_Logo
                _%282019%29.png' alt='' />
                <div className='header_input'>
                    <SearchIcon />
                    <input placeholder='Search Facebook' type='text' />
                </div>
            </div>
            <div className='header_middle'>
                <div className='header_option header_option--active'>
                    <HomeIcon fontSize='large' />
                </div>
                <div className='header_option'>
                    <OndemandVideoIcon fontSize='large' />
                </div>
                <div className='header_option'>
                    <StorefrontIcon fontSize='large' />
                </div>
                <div className='header_option'>
                    <GroupsIcon fontSize='large' />
                </div>
                <div className='header_option'>
                    <SportsEsportsIcon fontSize='large' />
                </div>
            </div>
            <div className='header_right'>
                    <AppsIcon className='abc' />
                    <MessageIcon className='abc' />
                    <NotificationsIcon className='abc' />
                <div className='header_info' style={{paddingLeft:"10px"}}>
                    <img style={{cursor: 'pointer'}} src={doha} alt='' />
                </div>
            </div>
        </div>
    )
}
export default Header;