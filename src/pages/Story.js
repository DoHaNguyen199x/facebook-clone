import {Avatar} from '@material-ui/core'
import '../style/Story.scss';
const Story = (props) => {
    return (
        <div className='story'>
            <Avatar className='story_avatar' src = {props.profileSrc} />
            <h5>{props.title}</h5>
            
        </div>
    )
}
export default Story;