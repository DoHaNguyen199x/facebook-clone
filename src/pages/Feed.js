import '../style/Feed.scss'
import StoryReel from './StoryReel';
import MessageSender from './MessageSender';
import Posts from './Posts';
import doha from '../components/doha.jpg';
const Feed = () => {
    return (
        <div className='feed'>
            <StoryReel />
            <MessageSender />
            <Posts userImage={doha} message="test 1" image={doha} userName="Đỗ Hà" />
            <Posts userImage={doha} message="test 2" image={doha} userName="Đỗ Hà" />
            <Posts userImage={doha} message="test 3" image={doha} userName="Đỗ Hà" />
        </div>
    )
}
export default Feed;